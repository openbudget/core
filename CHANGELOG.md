# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased](https://gitlab.com/openbudget/core/compare/v0.1.1...master)

## [0.1.1](https://gitlab.com/openbudget/core/compare/v0.1.0...v0.1.1)

### Added

- Install [ex_machina](https://github.com/thoughtbot/ex_machina) for creating test factory data. [!4](https://gitlab.com/openbudget/core/merge_requests/4)
- Install [ex_doc](https://github.com/elixir-lang/ex_doc) for generating documentation about the codebase. [!5](https://gitlab.com/openbudget/core/merge_requests/5)

## 0.1.0 - 2018-07-31

### Added

- This Project
- [ASDF version file](https://github.com/asdf-vm/asdf#the-tool-versions-file) for developer consistency. [!1](https://gitlab.com/openbudget/core/merge_requests/1)
- [Editor Config file](https://editorconfig.org/) for code editor consistency among developers. [!1](https://gitlab.com/openbudget/core/merge_requests/1)
- Install [Credo](http://credo-ci.org/) to enforce code consistency and uphold coding standards. [!1](https://gitlab.com/openbudget/core/merge_requests/1)
- [Code of Conduct](CODE_OF_CONDUCT.md) doc. [!1](https://gitlab.com/openbudget/core/merge_requests/1)
- [Contributing](CONTRIBUTING.md) doc. [!1](https://gitlab.com/openbudget/core/merge_requests/1)
- [Read Me](README.md) doc. [!1](https://gitlab.com/openbudget/core/merge_requests/1)
- Install [Coveralls](https://github.com/parroty/excoveralls) to check test coverage. [!1](https://gitlab.com/openbudget/core/merge_requests/1)
- [License](LICENSE) doc. [!1](https://gitlab.com/openbudget/core/merge_requests/1)
- This Changelog doc. [!1](https://gitlab.com/openbudget/core/merge_requests/1)
- Gitlab CI integration. [!2](https://gitlab.com/openbudget/core/merge_requests/2)
