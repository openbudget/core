defmodule OpenBudget.Factory do
  use ExMachina.Ecto, repo: OpenBudget.Repo

  alias OpenBudget.Authentication.User

  def user_factory do
    %User{
      username: sequence(:username, &"testuser#{&1}"),
      first_name: "Test",
      last_name: "User"
    }
  end
end
