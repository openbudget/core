defmodule OpenBudget.AuthenticationTest do
  use OpenBudget.DataCase
  import OpenBudget.Factory

  alias OpenBudget.Authentication
  alias OpenBudget.Authentication.User

  describe "users" do
    @valid_attrs %{first_name: "Test", last_name: "User", username: "testuser"}
    @update_attrs %{first_name: "Updated Test", last_name: "Updated User"}
    @invalid_attrs %{first_name: nil, last_name: nil, username: nil}

    test "list_users/0 returns all users" do
      user = insert(:user)
      assert Authentication.list_users() == [user]
    end

    test "get_user!/1 returns the user with given id" do
      user = insert(:user)
      assert Authentication.get_user!(user.id) == user
    end

    test "create_user/1 with valid data creates a user" do
      assert {:ok, %User{} = user} = Authentication.create_user(@valid_attrs)
      assert user.first_name == "Test"
      assert user.last_name == "User"
      assert user.username == "testuser"
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Authentication.create_user(@invalid_attrs)
    end

    test "update_user/2 with valid data updates the user" do
      user = insert(:user)
      assert {:ok, user} = Authentication.update_user(user, @update_attrs)
      assert %User{} = user
      assert user.first_name == "Updated Test"
      assert user.last_name == "Updated User"
    end

    test "update_user/2 with invalid data returns error changeset" do
      user = insert(:user)
      assert {:error, %Ecto.Changeset{}} = Authentication.update_user(user, @invalid_attrs)
      assert user == Authentication.get_user!(user.id)
    end

    test "delete_user/1 deletes the user" do
      user = insert(:user)
      assert {:ok, %User{}} = Authentication.delete_user(user)
      assert_raise Ecto.NoResultsError, fn -> Authentication.get_user!(user.id) end
    end

    test "change_user/1 returns a user changeset" do
      user = insert(:user)
      assert %Ecto.Changeset{} = Authentication.change_user(user)
    end
  end
end
