defmodule OpenBudget.Authentication do
  @moduledoc """
  Context for handling User Authentication.
  """

  import Ecto.Query, warn: false
  alias OpenBudget.Repo

  alias OpenBudget.Authentication.User

  @doc """
  Returns the list of users.

  ## Examples

      iex> list_users()
      [%User{}, ...]

  """
  def list_users do
    Repo.all(User)
  end

  @doc """
  Gets a single user.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

      iex> get_user!(123)
      %User{}

      iex> get_user!(456)
      ** (Ecto.NoResultsError)

  """
  def get_user!(id), do: Repo.get!(User, id)

  @doc """
  Creates a user.

  ## Examples

      iex> create_user(%{first_name: "Test", last_name: "User", username: "testuser123"})
      {:ok, %User{}}

      iex> create_user(%{first_name: "Test", last_name: "User", username: "existingusername"})
      {:error, %Ecto.Changeset{}}

      iex> create_user(%{first_name: nil, last_name: nil, username: nil})
      {:error, %Ecto.Changeset{}}

      iex> create_user(%{first_name: "", last_name: "", username: ""})
      {:error, %Ecto.Changeset{}}

  """
  def create_user(attrs \\ %{}) do
    %User{}
    |> User.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a user.

  ## Examples

      iex> update_user(user, %{first_name: "Updated Firstname", last_name: "Updated Lastname"})
      {:ok, %User{}}

      iex> update_user(user, %{first_name: nil, last_name: nil})
      {:error, %Ecto.Changeset{}}

      iex> update_user(user, %{first_name: "", last_name: ""})
      {:error, %Ecto.Changeset{}}

  """
  def update_user(%User{} = user, attrs) do
    user
    |> User.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a User.

  ## Examples

      iex> delete_user(user)
      {:ok, %User{}}

      iex> delete_user(user)
      {:error, %Ecto.Changeset{}}

  """
  def delete_user(%User{} = user) do
    Repo.delete(user)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking user changes.

  ## Examples

      iex> change_user(user)
      %Ecto.Changeset{source: %User{}}

  """
  def change_user(%User{} = user) do
    User.changeset(user, %{})
  end
end
