defmodule OpenBudget.Authentication.User do
  @moduledoc """
  A User in OpenBudget.

  It contains the following attributes:

  - `first_name` - First Name of the User
  - `last_name` - Last Name of the User
  - `username` - Identifier for the User. Must be unique.
  """

  use Ecto.Schema
  import Ecto.Changeset

  schema "users" do
    field(:first_name, :string)
    field(:last_name, :string)
    field(:username, :string)

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:username, :first_name, :last_name])
    |> validate_required([:username, :first_name, :last_name])
    |> unique_constraint(:username)
  end
end
